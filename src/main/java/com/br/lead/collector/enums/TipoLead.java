package com.br.lead.collector.enums;

public enum TipoLead {
    QUENTE,
    ORGANICO,
    FRIO,
}
