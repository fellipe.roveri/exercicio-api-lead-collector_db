package com.br.lead.collector.models;

import com.br.lead.collector.enums.TipoLead;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nome_completo")
    @Size(min = 8, max = 100, message = "O nome deve ter de 8 a 100 caracteres")
    private String nome;

    @Email(message = "O formato do e-mail é inválido.")
    private String email;

    private TipoLead tipoLead;

    @ManyToMany
    private List<Produto> produtos;

    public Lead() {
    }

    public Lead(String nome, String email, TipoLead tipoLead) {
        this.nome = nome;
        this.email = email;
        this.tipoLead = tipoLead;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public TipoLead getTipoLead() {
        return tipoLead;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTipoLead(TipoLead tipoLead) {
        this.tipoLead = tipoLead;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
